/*
    Copyright 2016 fishpepper <AT> gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    author: fishpepper <AT> gmail.com
*/
#ifndef HOPTABLE_H_
#define HOPTABLE_H_

#define FRSKY_DEFAULT_FSCAL_VALUE 0x00

#define FRSYK_TXID (0x301A)

// hoptable was generated with start=2, spacing=128
#define FRSKY_HOPTABLE { \
0x2, 0x82, 0x17, 0x97, 0x2c, 0xac, 0x41, 0xc1, 0x56, 0xd6, 0x6b, 0x1, \
0x81, 0x16, 0x96, 0x2b, 0xab, 0x40, 0xc0, 0x55, 0xd5, 0x6a, 0xea, \
0x7f, 0x14, 0x94, 0x29, 0xa9, 0x3e, 0xbe, 0x53, 0xd3, 0x68, 0xe8, \
0x7d, 0x12, 0x92, 0x27, 0xa7, 0x3c, 0xbc, 0x51, 0xd1, 0x66, 0xe6, \
0x7b, 0x10 \
};

#endif  // HOPTABLE_H_
